package br.com.project.restwithspringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.project.restwithspringboot.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

}
